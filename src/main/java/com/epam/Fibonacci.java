package com.epam;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
    private static int countOfOddFNumbers = 0;
    private static int countOfEvenFNumbers = 0;

    public static void percentageOfOddNumbers(int F1, int F2, int size) {
        int n0 = F1;
        int n1 = F2;
        int n2;
        //-----------------------------------------------
        //генеруємо числа Фібоначі
        List<Integer> fNumbers = new ArrayList<Integer>();
        fNumbers.add(n0);
        fNumbers.add(n1);
        System.out.print(n0 + " " + n1 + " ");
        for (int i = 3; i <= size; i++) {
            n2 = n0 + n1;
            fNumbers.add(n2);
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
        }
        System.out.println();
        //-----------------------------------------------
        //рахуємо к-сть непарних чисел
        for (int i = 0; i < fNumbers.size(); i++) {
            if (fNumbers.get(i) % 2 != 0) {
                countOfOddFNumbers++;
            }
        }
        //-------------------------------------------------
            //рахуємо к-сть парних чисел
        for (int i = fNumbers.size() - 1; i >= 0; i--) {
            if (fNumbers.get(i) % 2 == 0) {
                countOfEvenFNumbers++;
            }
        }
        System.out.println();
        //-----------------------------------------------------------------------------
        //вираховуємо відсоток непарних чисел Фібоначі
        int percentageOfOddNumber = (countOfOddFNumbers * 100) / (fNumbers.size() - 1);
        System.out.println("Percent of odd numbers: " + percentageOfOddNumber + "%");

        //вираховуємо відсоток парних чисел Фібоначі
        int percentageOfEvenNumbers = (countOfEvenFNumbers * 100) / (fNumbers.size() - 1);
        System.out.println("Percent of even numbers: " + (100 - percentageOfOddNumber) + "%");


    }
}
